FROM library/golang:1.13 AS builder

COPY . /src
RUN set -xe \
    && cd /src \
    && go mod download \
    && go build -o /tmp/server .

FROM gcr.io/distroless/base
COPY --from=builder /tmp/server /server

ENTRYPOINT ["/server", "-address=0.0.0.0", "-port=8080"]
