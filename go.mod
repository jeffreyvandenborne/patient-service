module gitlab.com/jvdnboe/patient-service

go 1.13

require (
	cloud.google.com/go v0.45.1
	cloud.google.com/go/pubsub v1.0.1 // indirect
	github.com/golang/protobuf v1.3.2
	github.com/google/uuid v1.1.1
	github.com/joho/godotenv v1.3.0
	google.golang.org/grpc v1.21.1
)
