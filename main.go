package main

import (
	"cloud.google.com/go/spanner"
	"context"
	"flag"
	"fmt"
	"github.com/joho/godotenv"
	"gitlab.com/jvdnboe/patient-service/patient"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
)

var (
	address string
	port    int
)

const (
	SpannerDatabaseEnvKey = "SPANNER_DB_URL"
)

func init() {
	flag.IntVar(&port, "port", 7777, "port for grpc")
	flag.StringVar(&address, "address", "127.0.0.1", "listen address")
}

func connectSpannerClient(dbURL string) (*spanner.Client, error) {
	ctx := context.Background()
	client, err := spanner.NewClient(ctx, dbURL)
	if err != nil {
		return nil, err
	}

	return client, nil
}

func main() {
	flag.Parse()
	err := godotenv.Load()
	if err != nil {
		log.Fatal("error loading .env file")
	}

	listener, err := net.Listen("tcp", fmt.Sprintf("%s:%d", address, port))
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "error occurred while setting up the listener: %v\n", err)
	}

	spannerURL := os.Getenv(SpannerDatabaseEnvKey)
	spannerClient, err := connectSpannerClient(spannerURL)
	if err != nil {
		log.Fatalf("could not connect to spanner instance: %s", spannerURL)
	}

	grpcServer := grpc.NewServer()

	patientServer := &patient.Server{SpannerClient: spannerClient}
	patient.RegisterPatientServer(grpcServer, patientServer)

	if err := grpcServer.Serve(listener); err != nil {
		log.Fatalf("Failed to serve grpc server: %s\n", err)
	}
}
