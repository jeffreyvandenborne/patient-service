package patient

import (
	"cloud.google.com/go/spanner"
	"context"
	"fmt"
	"github.com/google/uuid"
)

type Server struct {
	SpannerClient *spanner.Client
}

func (server *Server) GetPatient(ctx context.Context, in *Identifier) (*PatientMessage, error) {
	columns := []string{
		"FirstName",
		"LastName",
		"PatientId",
	}
	row, err := server.SpannerClient.Single().ReadRow(ctx, "Patients", spanner.Key{in.PatientId}, columns)
	if err != nil {
		return nil, err
	}

	var patientMessage = new(PatientMessage)
	err = row.ToStruct(patientMessage)
	if err != nil {
		return nil, err
	}

	return patientMessage, nil
}

func (server *Server) CreatePatient(ctx context.Context, patientMessage *PatientMessage) (*PatientMessage, error) {
	patientId, err := uuid.NewUUID()
	if err != nil {
		return nil, err
	}

	patientMessage.PatientId = patientId.String()
	newPatientMutation := spanner.InsertMap("Patients", map[string]interface{}{
		"FirstName": patientMessage.GetFirstName(),
		"LastName": patientMessage.GetLastName(),
		"PatientId": patientMessage.GetPatientId(),
	})

	_, err = server.SpannerClient.Apply(ctx, []*spanner.Mutation{newPatientMutation})
	if err != nil {
		return nil, fmt.Errorf("error while applying mutation: %v", err)
	}

	return patientMessage, nil
}
